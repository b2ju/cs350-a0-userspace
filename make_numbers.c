#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <math.h>

int main(int argc, char *argv[]) {

   printf("Program name %s\n", argv[0]);

   long n = atol(argv[1]);
   long lo = atol(argv[2]);
   long hi = atol(argv[3]);

   FILE *f = fopen("log.txt", "wb");

   if ((1 > n) || (n > INT_MAX)) {
      exit(1);
   } else if ((INT_MIN > hi) || (hi > INT_MAX)) {
      exit(1);
   } else if ((INT_MIN > lo) || (lo > INT_MAX)) {
      exit(1);
   } else if (lo > hi) {
      exit(1);
   }

   srand(getpid());

   fprintf(f, "%ld\n", n);
   for (int i = 0; i < n; i++)
   {
      int num = (rand() % (hi - lo + 1)) + lo;
      fprintf(f, "%d\n", num);
   }

   fclose(f);
   exit(0);
}
