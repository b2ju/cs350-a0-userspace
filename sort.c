#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>

int compare (const void *a, const void *b) {
   return (*(int*)a - *(int*)b);
}

int main(int argc, char *argv[])
{

    // open files
    FILE *log = fopen("log.txt", "r");
    FILE *sort = fopen("sorted.txt", "w");

    // scan log.txt for numbers and put in array
    int num;
    fscanf(log, "%d", &num);
    int myArray[num];
    for (int i = 0; i < num; i++)
    {
        int temp;
        fscanf(log, "%d", &temp);
        myArray[i] = temp;
    }

    struct timeval start_time;
    gettimeofday(&start_time, NULL);

    qsort(myArray, num, sizeof(int), compare);

    struct timeval end_time;
    gettimeofday(&end_time, NULL);
    printf("%ld\n", (long)(end_time.tv_usec - start_time.tv_usec));
    
    // print sorted array to sorted.txt
    fprintf(sort, "%d\n", num);
    for (int i = 0; i < num; i++)
    {
        fprintf(sort, "%d\n", myArray[i]);
    }

    // close files
    fclose(log);
    fclose(sort);
    exit(0);
}
